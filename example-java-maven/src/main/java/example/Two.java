package example;

/**
 * 
 * @author Julien
 *
 */
public class Two {
	String message = "foo";
	public static final String ERROR = "ERROR";
	public static final String SUCCESS = "SUCCESS";

	/**
	 * 
	 * @param one classe One
	 * @return "ERROR" or "SUCCESS"
	 */
	public String comp(One one) {
		if (this.compareTo(one) == -1)
			return ERROR;
		return SUCCESS;
	}

	private int compareTo(One one) {
		return message.compareTo(one.foo());
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Two other = (Two) obj;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message)) {
			return false;
		}
		return true;
	}
}
