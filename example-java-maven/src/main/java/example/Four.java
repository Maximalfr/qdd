package example;

public class Four {
	String message;
	
	public Four() {
		this.message = "foo";
	}
	
	public Four(String msg) {
		this.message = msg;
	}

	public String getMessage() {
		return message;
	}
	
	// Maybe rename it to setMessage
	public void setString(String msg) {
		this.message = msg;
	}

	public boolean isMessageEmpty() {
		return getMessage().isEmpty();
	}
}
