package example;

import java.util.logging.Logger;

/**
 * 
 * @author Julien
 *
 */
public class Three {
	String message = "foo";
	private final Logger log = Logger.getLogger(Three.class.getName());
	
	/**
	 * 
	 * @param two
	 * @return les messages concaténés
	 */
	public String concat(Two two) {
		StringBuilder sb = new StringBuilder(message);
		return sb.append(two.message).toString();
	}				   

	public String concat2(One one) {
		StringBuilder sb = new StringBuilder(message);
		return sb.append(one.message).toString();
	}

	public String concat3(One one, Two two) {
		StringBuilder sb = new StringBuilder(message);
		String result =  sb.append("_").append(one.foo()).append("-").append(two.message).toString();
		log.info(result);
		return result;
	}
}
