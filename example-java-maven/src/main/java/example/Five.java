package example;

public class Five {
	//private String message;  // Unused method. To delete

	public String intToString(int unEntier) {
		return String.valueOf(unEntier);
	}

	public String intToString1(int unEntier) {
		return Integer.toString(unEntier);
	}
	
	// Code duplicated

	public String intToString2(int unEntier) {
		return intToString(unEntier);
	}

	public String intToString3(int unEntier) {
		return intToString(unEntier);
	}
}
