package example;

public class Seven extends Two {
	String sevenMessage = "foo";

	public String longString(int facteur) {
		StringBuilder res = new StringBuilder();
		for (int i = 0; i < facteur; i++) {
			res.append(message);
		}
		return res.toString();
	}

	public void setMessage(String message) {
		this.sevenMessage = message;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((sevenMessage == null) ? 0 : sevenMessage.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Seven other = (Seven) obj;
		if (sevenMessage == null) {
			if (other.sevenMessage != null) {
				return false;
			}
		} else if (!sevenMessage.equals(other.sevenMessage)) {
			return false;
		}
		return true;
	}
	
	
}
