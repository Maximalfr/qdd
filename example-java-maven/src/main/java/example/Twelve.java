package example;

public class Twelve extends Thread implements Runnable {
	boolean stopped;

	public Twelve() {
		this.start();
		this.stopped = false;
	}

	@Override
	public void run() {
		// Utilise stopped pour arrêter le process en cours
	}

	public void arret() {
		if (this.isAlive()) {
			//notify(); Ne sert pas dans ce cas parce qu'aucun wait est utilisé
			this.stopped = true;
		}
	}
}