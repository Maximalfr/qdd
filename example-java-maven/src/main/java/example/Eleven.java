package example;

public class Eleven {
	
	public boolean testValue(String msg) {
		return msg.equals("ABC") || msg.equals("DEF") || "XYZ".equals(msg) || msg.endsWith("CBA");
			
		// Les tests étaient de toute façon faux puisque que l'on test l'intégralité de msg avec equals
	}

}
