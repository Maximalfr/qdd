package example;

import java.io.BufferedReader; // io est deprecated passer à nio
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Six {
	private String fileName;
	private List<String> propertyList;
	private final Logger log = Logger.getLogger(Six.class.getName());

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void readTheFile() {
		Path path = Paths.get(this.getFileName());
		try (BufferedReader reader = Files.newBufferedReader(path);) {
			String line = reader.readLine();
			while (line != null) {
				line = reader.readLine();
			}
		} catch (IOException e) {
			log.log(Level.SEVERE, e.getMessage());
		}
	}

	public void readTheFile2() throws IOException {
		for (String property : propertyList) {
			try (OutputStream stream = new FileOutputStream(this.getFileName()); ) {
				stream.write(property.getBytes());

			} catch (FileNotFoundException fnfe) {
				log.log(Level.SEVERE, fnfe.getMessage());
				// Nous pouvons aussi afficher le stacktrace dans le logger
			}
		}
	}
}
