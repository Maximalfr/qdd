package test;

import static org.junit.Assert.*;

import org.junit.Test;

import example.One;

public class OneTest {

	@Test
	/**
	 * One.foo() should return "foo"
	 */
	public void fooTest() {
		One one = new One();
		assertEquals("foo",one.foo());
	}

}
