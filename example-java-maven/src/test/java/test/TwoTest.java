package test;

import static org.junit.Assert.*;

import org.junit.Test;

import example.One;
import example.Two;

public class TwoTest {

	@Test
	/**
	 * One should be comparable to Two
	 */
	public void compTest() {
		One one = new One();
		Two two = new Two();
		assertEquals(Two.SUCCESS, two.comp(one));
	}

}
