package test;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.Before;

import example.One;
import example.Three;
import example.Two;

public class ThreeTest {
	
	private One one;
	private Two two;
	private Three three;
	
	@Before
	public void init() {
		this.one = new One();
		this.two = new Two();
		this.three = new Three();
	}

	@Test
	public void concatTest() {		
		assertEquals("foofoo", this.three.concat(this.two));
		assertEquals("foofoo", this.three.concat2(this.one));
		assertEquals("foo_foo-foo", this.three.concat3(this.one,  this.two));
	}

}
