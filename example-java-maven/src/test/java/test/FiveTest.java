package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import example.Five;

public class FiveTest {
	
	private Five five;
	
	@Before
	public void init() {
		this.five = new Five();
	}

	@Test
	public void intToStringTest() {
		assertEquals("1",this.five.intToString(1));
		assertEquals("444",this.five.intToString(444));
	}
	
	@Test
	public void intToString1Test() {
		assertEquals("1",this.five.intToString1(1));
		assertEquals("444",this.five.intToString1(444));
	}
}
