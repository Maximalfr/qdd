package test;

import static org.junit.Assert.*;
import org.junit.Before;

import org.junit.Test;

import example.Four;

public class FourTest {
	
	private Four four;
	
	@Before
	public void init() {
		this.four = new Four("foo");
	}

	@Test
	public void assessorTest() {
		assertEquals("foo", this.four.getMessage());
		this.four.setString("chgfoo");
		assertNotEquals("foo", this.four.getMessage());
		assertEquals("chgfoo", this.four.getMessage());
	}
	
	@Test
	public void messageEmpty() {
		assertFalse(this.four.isMessageEmpty());
		this.four.setString("");
		assertTrue(this.four.isMessageEmpty());
	}

}
