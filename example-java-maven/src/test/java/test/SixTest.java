package test;

import static org.junit.Assert.*;

import org.junit.Test;

import example.Six;

public class SixTest {

	@Test
	public void filenameTest() {
		Six six = new Six();
		assertNull(six.getFileName());
		six.setFileName("file");
		assertEquals("file", six.getFileName());
	}

}
